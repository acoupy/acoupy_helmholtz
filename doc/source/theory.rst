.. _theory:

===================
Theory
===================

This package aims to solve for the spatial part :math:`p` of the harmonic steady state pressure field over a domain
:math:`\Omega`. The governing equation for :math:`p` is the Helmholtz equation. Dirichlet, Neumann and Robin boundary
conditions can be applied as well as a source term.

.. NOTE::
   Currently, only uniform boundary conditions can be applied, while there is not code support for source terms.

----------------------
The Equation
----------------------

Let :math:`\Omega` be an open in :math:`\mathbb{R}^{N}`, where :math:`N` is either :math:`1`, :math:`2` or :math:`3`.
Let :math:`\partial\Omega` be the boundary of :math:`\Omega` and let's denote with :math:`\partial\Omega_{m}` with
:math:`m=0,1,\dots,M-1` the :math:`M` elements of a partition of :math:`\Omega`. Finally, let's denote with
:math:`\Omega^{\prime}` the closure of of :math:`\Omega`, i.e. :math:`\Omega^{\prime} = \partial\Omega \cup \Omega`.

We are then induced to consider the Sobolev space :math:`V` of all complex valued functions over :math:`\Omega^{\prime}`
which are

  - square integrable together with all their derivatives up to the the order :math:`N-1`; and
  - :math:`0` over all the :math:`\partial\Omega_{m}` for which the value of the unknown pressure field is already
    known (Dirichlet boundaries).

We denote with :math:`V^{\prime}` the dual of :math:`V`.

The governing equation is:

.. math::

   \gamma\nabla^{2}p + \beta p = q

Where :math:`p : \Omega^{\prime} \rightarrow \mathbb{C}` is the unknown steady state complex pressure field.

:math:`q \in V^{\prime}` is the source term. :math:`\gamma` and :math:`\beta` are complex scalars defined as follows:

.. math::

  \begin{flalign}
    \gamma = 1 + j k \frac{\eta}{c} \\
    \beta = k^2 - jk \frac{\alpha}{c}
  \end{flalign}

With :math:`j` the imaginary unit. The material properties are:

  - :math:`c` the phase speed of sound in the material;
  - :math:`\rho` the mass density of the material (this is is used in the boundary conditions);
  - :math:`\eta` is a damping parameter related to the material viscosity; and
  - :math:`\alpha` is a reactive damping parameter.

To obtain the ordinary lossless Helmholtz equation set :math:`\eta=\alpha=0`.

The only simulation property (other than the domain and boundary partition) is the frequency :math:`f`, which sets the
wavenumber :math:`k` above through :math:`c`: :math:`k=\frac{2\pi f}{c}`.

----------------------
Boundary Conditions
----------------------

~~~~~~~~~~~~~~~~~~~~~~
Dirichlet
~~~~~~~~~~~~~~~~~~~~~~

Let :math:`D` be a set of indexes selecting the Dirichlet boundaries. For each :math:`d \in D` we define
:math:`p_{d}: \partial\Omega_{d} \rightarrow \mathbb{C}`. For each :math:`d \in D` we hence prescribe:

.. math::

   p\left(\mathbf{x}\right) = p_{d}\left(\mathbf{x}\right) \space \forall \mathbf{x} \in \partial\Omega_{d}

~~~~~~~~~~~~~~~~~~~~~~
Neumann
~~~~~~~~~~~~~~~~~~~~~~

Let :math:`U` be a set of indexes selecting the Neumann boundaries. For each :math:`u \in U` we define
:math:`v_{u}: \partial\Omega_{u} \rightarrow \mathbb{C}` as the normal particle velocity field to the boundary
:math:`\partial\Omega_{u}`, where the normal direction is defined by the outward normal unit vector
:math:`\hat{\mathbf{n}}`. For each :math:`u \in U` we hence prescribe:

.. math::

   \left[ \nabla p \cdot \hat{\mathbf{n}} \right] \left(\mathbf{x}\right) = -j \omega \rho v_{u}\left(\mathbf{x}\right) \space \forall \mathbf{x} \in \partial\Omega_{u}

With :math:`\omega = 2\pi f`.

~~~~~~~~~~~~~~~~~~~~~~
Robin
~~~~~~~~~~~~~~~~~~~~~~

Let :math:`R` be a set of indexes selecting the Robin boundaries. For each :math:`r \in R` we define:
:math:`z_{r}: \partial\Omega_{r} \rightarrow \mathbb{C}` as the specific acoustic impedance of
:math:`\partial\Omega_{r}`. Additionally, we define :math:`v_{r}: \partial\Omega_{r} \rightarrow \mathbb{C}` as the
normal particle velocity field to the boundary :math:`\partial\Omega_{r}`, where the normal direction is defined by
the outward normal unit vector :math:`\hat{\mathbf{n}}`. For each :math:`r \in R` we hence prescribe:

.. math::

   \left[ \nabla p \cdot \hat{\mathbf{n}} \right] \left(\mathbf{x}\right) = -j \omega \rho \left[\frac{p\left(\mathbf{x}\right)}{z_{r}\left(\mathbf{x}\right)} + v_{r}\left(\mathbf{x}\right) \right] \space \forall \mathbf{x} \in \partial\Omega_{r}

With :math:`\omega = 2\pi f`.


----------------------
Weak Form
----------------------

The weak form is found by applying the functionals on each side of the PDE to any test function :math:`\phi \in V`
through the Sobolev's space inner product. This results in a integral equation that can be simplified by integrating
by parts and substituting the boundary conditions in the resulting terms. This is the form that is handled by the
software and it is reported below for completeness.

.. math::

  \begin{flalign}
    \gamma \int_{\Omega} \nabla p \cdot \nabla \phi^\star d\mathbf{x} + \\
    - \beta \int_{\Omega} p \phi^{\star} d\mathbf{x} + \\
    +g \sum_{r \in R} \int_{\partial \Omega_{r}} \frac{p}{z_r} \phi^\star ds  = \\
    = -\int_{\Omega} q \phi^{\star} d\mathbf{x} + \\
    -g \sum_{u \in U} \int_{\partial \Omega_{u}} v_u \phi^\star ds + \\
    -g \sum_{r \in R} \int_{\partial \Omega_{r}} v_r \phi^\star ds
  \end{flalign}

Where :math:`g = j\omega\rho\gamma` is the flux constant. This form can be summarised as follows:

.. math::

    a \left(p, \phi \right) = L \left(\phi \right)

With :math:`a` and :math:`L` being the bilinear and linear forms respectively.

.. NOTE::
   The Dirichlet terms do not appear in the weak form as the integral terms are :math:`0` on the Dirichlet boundary.
   They are dealt with separately in the software.

----------------------
References
----------------------

  - `Analisi Matematica di Base <https://www.mheducation.it/analisi-matematica-di-base-2-ed-9788838666599-italy>`_;
  - `Elmer Models Manual - Chapter 14 <https://www.nic.funet.fi/index/elmer/doc/ElmerModelsManual.pdf#chapter.14>`_;
  - `Elmer Models Manual - Chapter 12 <https://www.nic.funet.fi/index/elmer/doc/ElmerModelsManual.pdf#chapter.12>`_;
  - `Computational Acoustics of Noise Propagation in Fluids <https://link.springer.com/book/10.1007%2F978-3-540-77448-8>`_;
  - `Solving PDEs in Python <https://link.springer.com/book/10.1007/978-3-319-52462-7>`_;
  - `Complex Helmholtz Equation with Neumann Conditions <https://fenicsproject.discourse.group/t/complex-helmholtz-equation-with-neumann-conditions/7007/13>`_;
  - `Fundamentals of Acoustics <https://www.wiley.com/en-gb/Fundamentals+of+Acoustics,+4th+Edition-p-9780471847892>`_;
  - `DeltaEC Users Guide <https://www.lanl.gov/org/ddste/aldps/materials-physics-applications/condensed-matter-magnet-science/thermoacoustics/_assets/docs/UsersGuide.pdf#section.2.1>`_;
  - `Intro to FEniCS - Part 2 <https://computational-acoustics.gitlab.io/website/posts/31-intro-to-fenics-part-2/>`_;
