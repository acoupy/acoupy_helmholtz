acoupy\_helmholtz package
=========================

Submodules
----------

acoupy\_helmholtz.solver module
-------------------------------

.. automodule:: acoupy_helmholtz.solver
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: acoupy_helmholtz
   :members:
   :undoc-members:
   :show-inheritance:
