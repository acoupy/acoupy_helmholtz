.. acoupy_helmholtz documentation master file, created by
   sphinx-quickstart on Sat Sep 24 13:25:03 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to acoupy_helmholtz's documentation!
============================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

``acoupy_helmholtz`` is a ``Python`` package for the solution of the Acoustics Helmholtz equation. The solution is
computed with the ``FEM`` method by using ``FEniCS``. Honestly speaking, ``acoupy_helmholtz`` is little more of a
wrapper around the relevant ``FEniCS`` functions.

===================
Theory
===================

See :ref:`theory`.

===================
Installation
===================

----------------------
Dependencies
----------------------

Most dependencies are handled automatically. ``FEniCS`` instead must be installed beforehand. For more information see
the `FEniCS documentation <https://fenics.readthedocs.io/en/latest/installation.html>`_.

On some Linux systems you might want to install other dependencies beforehand as well to ensure they are all linked to
the same libraries on the system. See below for specific instructions.

~~~~~~~~~~~~~~~~~~~~~~
On Ubuntu
~~~~~~~~~~~~~~~~~~~~~~

Install the following packages:

.. code:: bash

   sudo apt install python3-h5py python3-meshio

----------------------
Environment
----------------------

If you wish to use a virtual environment you must create it so that the system packages installed above
(including ``FEniCS``) are available. Use:

.. code:: bash

   python3 -m venv --system-site-packages venv


And activate your environment.

----------------------
Installation
----------------------

Use `pip` to install the software on your target environment:

.. code:: bash

   pip install git+https://gitlab.com/acoupy/acoupy_helmholtz.git


===================
Quickstart
===================

The quickest way to start is by installing `acoupy_meshutil <https://gitlab.com/acoupy/acoupy_meshutil>`_ and using
its built-in meshes. Install ``acoupy_meshutil``:

.. code:: bash

   pip install git+https://gitlab.com/acoupy/acoupy_meshutil.git

Them, solve a simple pulsating sphere case:

.. code:: python

   from acoupy_meshutil import builtin
   import fenics
   from acoupy_helmholtz import solver

   # Create a loader for the default sphere cut mesh. Get the mesh and define measures.
   # The sphere cut is a spherical mesh with a smaller sphere cut from the center. All information is in the
   # ``metadata``
   reader_obj, metadata = builtin.get_sphere_cut_mesh()
   mesh, body_markers, sub_domains_markers = reader_obj.get_mesh()
   dx_volumes = fenics.Measure('dx', domain=mesh, subdomain_data=body_markers)
   ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[0])

   # Solve a 1 kHz problem by applying a uniform velocity to the inner sphere shell and spherical wave impedance to the
   # outer sphere shell.

   medium = solver.MediumSettings()
   frequency = 1000.0
   wave_number = 2.0 * np.pi * frequency / medium.speed_of_sound
   source_velocity = (1.0 + 1.0j) / np.sqrt(2.0)
   outlet_impedance = (
          medium.density * medium.speed_of_sound * (1.0j * wave_number * metadata['outer_radius']) /
          (1.0 + 1.0j * wave_number * metadata['outer_radius'])
   )

   solution = solver.solve(
      mesh=mesh,
      simulation_settings=solver.SimulationSettings(frequency=frequency, medium_settings=medium),
      main_domain_settings=solver.MainDomainSettings(measure=dx_volumes),
      element_settings=solver.ElementSettings(degree=2),
      solver_settings=solver.SolverSettings(linear_solver='mumps'),
      uniform_neumann_bcs=solver.UniformNeumannBCS(
          [
              solver.UniformNeumannBC(velocity=source_velocity, measure=ds_surfaces(1))
          ]
      ),
      uniform_robin_bcs=solver.UniformRobinBCS(
          [
              solver.UniformRobinBC(impedance=outlet_impedance, velocity=0.0, measure=ds_surfaces(0))
          ]
      ),
   )

   # ``solution`` is an ordinary ``fenics.Function`` you can use as you want.