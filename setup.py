import setuptools

# Read the README.md file to add it to the long_description attribute below
with open('README.md', 'r', encoding='utf-8') as fh:
    long_description = fh.read()

setuptools.setup(
    name='acoupy_helmholtz',
    version='0.0.3',
    author='Stefano Tronci',
    author_email='stefano.tronci@protonmail.com',
    description='FEniCS based Acoustics Helmholtz solver.',
    long_description=long_description,
    long_description_content_type='text/markdown',
    url='https://gitlab.com/acoupy/acoupy_helmholtz',
    packages=setuptools.find_packages(),
    classifiers=[
        'Programming Language :: Python :: 3',
        'Operating System :: OS Independent',
    ],
    python_requires='>=3.6',
    install_requires=[
        'numpy',
        'dataclasses',
    ],
    extras_require={
        'doc': [
            'Sphinx!=5.2.0.post0',
            'karma-sphinx-theme'
        ],
        'tests': [
            'acoupy_meshutil@git+https://gitlab.com/acoupy/acoupy_meshutil.git',
        ]
    },
)
