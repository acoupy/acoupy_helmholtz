import unittest
from acoupy_meshutil import builtin
from acoupy_helmholtz import solver
import fenics
import numpy as np


class TestSolver(unittest.TestCase):

    def test_solver(self) -> None:
        reader_obj, metadata = builtin.get_sphere_cut_mesh()
        self.assertTrue(
            'outer_radius' in metadata.keys(),
            '`outer_radius` is not in the metadata. Did something change in `acoupy_meshutil`?'
        )
        self.assertTrue(
            'inner_radius' in metadata.keys(),
            '`inner_radius` is not in the metadata. Did something change in `acoupy_meshutil`?'
        )

        tags = reader_obj.get_cell_tags()
        self.assertTrue(0 in tags.keys(), '`0` is not in the `tags`. Did something change in `acoupy_meshutil`?')
        self.assertTrue(1 in tags.keys(), '`1` is not in the `tags`. Did something change in `acoupy_meshutil`?')
        self.assertTrue(tags[0] == ['outer'], '`tags[0] != [''outer'']`. Did something change in `acoupy_meshutil`?')
        self.assertTrue(tags[1] == ['inner'], '`tags[0] != [''inner'']`. Did something change in `acoupy_meshutil`?')

        mesh, body_markers, sub_domains_markers = reader_obj.get_mesh()
        dx_volumes = fenics.Measure('dx', domain=mesh, subdomain_data=body_markers)
        ds_surfaces = fenics.Measure('ds', domain=mesh, subdomain_data=sub_domains_markers[0])

        medium = solver.MediumSettings()
        frequency = 1000.0
        wave_number = 2.0 * np.pi * frequency / medium.speed_of_sound
        source_velocity = (1.0 + 1.0j) / np.sqrt(2.0)
        outlet_impedance = (
                medium.density * medium.speed_of_sound * (1.0j * wave_number * metadata['outer_radius']) /
                (1.0 + 1.0j * wave_number * metadata['outer_radius'])
        )

        solution = solver.solve(
            mesh=mesh,
            simulation_settings=solver.SimulationSettings(frequency=frequency, medium_settings=medium),
            main_domain_settings=solver.MainDomainSettings(measure=dx_volumes),
            element_settings=solver.ElementSettings(degree=2),
            solver_settings=solver.SolverSettings(linear_solver='mumps'),
            uniform_neumann_bcs=solver.UniformNeumannBCS(
                [
                    solver.UniformNeumannBC(velocity=source_velocity, measure=ds_surfaces(1))
                ]
            ),
            uniform_robin_bcs=solver.UniformRobinBCS(
                [
                    solver.UniformRobinBC(impedance=outlet_impedance, velocity=0.0, measure=ds_surfaces(0))
                ]
            ),
        )

        u_re, u_im = solution.split(deepcopy=True)

        # Inspired by this, but different: https://fenicsproject.org/pub/tutorial/html/._ftut1019.html
        # The solutions can be called to interpolate over values: just leverage that.
        mesh_nodes = mesh.coordinates()
        u_at_nodes = np.zeros(mesh_nodes.shape[0], dtype=complex)
        for r in range(mesh_nodes.shape[0]):
            u_at_nodes[r] = u_re(*mesh_nodes[r, :]) + 1j * u_im(*mesh_nodes[r, :])

        # Exact solution
        radii = np.sqrt(np.sum(mesh_nodes ** 2, axis=1))
        source_impedance = (
                medium.density * medium.speed_of_sound * (1.0j * wave_number * metadata['inner_radius']) /
                (1.0 + 1.0j * wave_number * metadata['inner_radius'])
        )
        u_exact_at_nodes = (
            source_impedance * metadata['inner_radius'] * source_velocity *
            np.exp(-1j * wave_number * (radii - metadata['inner_radius'])) / radii
        )

        error = u_at_nodes / u_exact_at_nodes
        error_db = 20 * np.log10(np.abs(error))
        error_deg = np.rad2deg(np.angle(error))
        self.assertTrue(np.all(np.abs(error_db) <= 0.5), 'Some solution values exceed 0.5 dB error!')
        self.assertTrue(np.all(np.abs(error_deg) <= 1.0), 'Some solution values exceed 1 deg error!')


if __name__ == '__main__':
    unittest.main()
