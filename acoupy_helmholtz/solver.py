import typing
import dataclasses
import numpy as np
import fenics


@dataclasses.dataclass
class ElementSettings:
    """
    Dataclass to specify finite element settings. By default, it provides the settings for second order P elements.

    :Example:

    >>> from acoupy_helmholtz import solver
    >>> # Let's make a first order Lagrange element specification:
    >>> element_settings = solver.ElementSettings(family='Lagrange', degree=1)
    """
    family: str = 'P'
    """Family of the element (``str``). By default, ``P``. This argument is the same as the ``family`` arguments of
    :class:`fenics.FiniteElement`'s constructor.
    """
    degree: int = 2
    """Degree of the element (``int``). By Default, ``2``. This argument is the same as the ``degree`` arguments of
    :class:`fenics.FiniteElement`'s constructor.
    """


@dataclasses.dataclass
class MediumSettings:
    """
    Dataclass to specify medium settings. By default, it provides the settings for lossless air at 20 Degree Celsius.

    :Example:

    >>> from acoupy_helmholtz import solver
    >>> # Let's specify medium parameters for a lossless model of air at 0 Celsius degrees
    >>> medium_settings = solver.MediumSettings(speed_of_sound=331.5, density=1.293)
    """
    speed_of_sound: float = 343.0
    """Speed of sound of the medium in :math:`\\frac{\\text{m}}{\\text{s}}` (``float``). By default, ``343.0``."""
    density: float = 1.205
    """Density of the medium in :math:`\\frac{\\text{kg}}{\\text{m}^3}` (``float``). By default, ``1.205``."""
    damping_eta: float = 0.0
    """:math:`\\eta` damping parameter in :math:`\\frac{\\text{rad}}{\\text{s}} \\text{m}^2` (``float``). 
    By default, ``0``. See
    `Intro to FEniCS - Part 2 <https://computational-acoustics.gitlab.io/website/posts/31-intro-to-fenics-part-2/>`_.
    """
    damping_alpha: float = 0.0
    """:math:`\\alpha` damping parameter in :math:`\\frac{\\text{rad}}{\\text{s}}` (``float``). By default, ``0``. See
    `Intro to FEniCS - Part 2 <https://computational-acoustics.gitlab.io/website/posts/31-intro-to-fenics-part-2/>`_.
    """


@dataclasses.dataclass
class SimulationSettings:
    """
    Dataclass to specify the simulation settings. By default, it provides the settings for a trivial null solution in
    the default medium. For medium settings see :class:`acoupy_helmholtz.solver.MediumSettings`.

    :Example:

    >>> from acoupy_helmholtz import solver
    >>> # Let's specify a 440 Hz simulation in the default medium.
    >>> simulation_settings = solver.SimulationSettings(frequency=440)
    """
    frequency: float = 0.0
    """The frequency of the simulation in :math:`\\text{Hz}` (``float``). By default, ``0``."""
    medium_settings: MediumSettings = MediumSettings()
    """The medium settings (:class:`acoupy_helmholtz.solver.MediumSettings`)."""


@dataclasses.dataclass
class MainDomainSettings:
    """
    Dataclass to specify the settings of the main domain of a simulation. The settings are specified as the measure
    of the domain.
    """
    measure: fenics.Measure
    """The measure of the domain to which the equation is applied (:class:`fenics.Measure`)."""


@dataclasses.dataclass
class UniformRobinBC:
    """
    Dataclass to specify the settings of a uniform Robin boundary condition. The boundary is identified by its measure.
    """
    impedance: complex
    """The specific acoustic impedance of the boundary in :math:`\\text{Rayl}_{\\text{MKS}}` (``complex``)."""
    velocity: complex
    """The normal velocity of the boundary in :math:`\\frac{\\text{m}}{\\text{s}}` (``complex``)."""
    measure: fenics.Measure
    """The measure of the boundary to which the condition is applied (:class:`fenics.Measure`)."""


def _make_dfl_uniform_robin_bcs() -> typing.List[UniformRobinBC]:
    return []


@dataclasses.dataclass
class UniformRobinBCS:
    """
    Dataclass to specify a set of uniform Robin boundary conditions.
    """
    bcs: typing.List[UniformRobinBC] = dataclasses.field(default_factory=_make_dfl_uniform_robin_bcs)
    """The list of boundary conditions (``List`` of :class:`acoupy_helmholtz.solver.UniformRobinBC`). 
    By default it is empty.
    """


@dataclasses.dataclass
class UniformNeumannBC:
    """
    Dataclass to specify a uniform Neumann boundary condition. The boundary is identified by its measure.
    """
    velocity: complex
    """The normal velocity of the boundary in :math:`\\frac{\\text{m}}{\\text{s}}` (``complex``)."""
    measure: fenics.Measure
    """The measure of the boundary to which the condition is applied (:class:`fenics.Measure`)."""


def _make_dfl_uniform_neumann_bcs() -> typing.List[UniformNeumannBC]:
    return []


@dataclasses.dataclass
class UniformNeumannBCS:
    """
    Dataclass to specify a set of uniform Neumann boundary conditions.
    """
    bcs: typing.List[UniformNeumannBC] = dataclasses.field(default_factory=_make_dfl_uniform_neumann_bcs)
    """The list of boundary conditions (``List`` of :class:`acoupy_helmholtz.solver.UniformNeumannBC`). 
    By default it is empty.
    """


@dataclasses.dataclass
class UniformDirichletBC:
    """"
    Dataclass to specify a uniform Dirichlet boundary condition. The boundary is identified by a subdomain.
    """
    pressure: complex
    """The boundary pressure value in :math:`\\text{Pa}` (``complex``)."""
    subdomain: fenics.SubDomain
    """The subdomain to which the condition is applied (:class:`fenics.SubDomain`)."""


def _make_dfl_uniform_dirichlet_bcs() -> typing.List[UniformDirichletBC]:
    return []


@dataclasses.dataclass
class UniformDirichletBCS:
    """
    Dataclass to specify a set of uniform Dirichlet boundary conditions.
    """
    bcs: typing.List[UniformDirichletBC] = dataclasses.field(default_factory=_make_dfl_uniform_dirichlet_bcs)
    """The list of boundary conditions (``List`` of :class:`acoupy_helmholtz.solver.UniformDirichletBC`). 
    By default it is empty.
    """


@dataclasses.dataclass
class SolverSettings:
    """
    Dataclass to specify the solver settings.
    """
    linear_solver: str
    """The linear solver method (``str``). Use :func:`fenics.list_linear_solver_methods` to list all direct solver 
    methods. Use :func:`fenics.list_krylov_solver_methods` to list all iterative solver methods.
    """
    preconditioner: typing.Optional[str] = None
    """The preconditioning method (``Optional[str]``). Use :func:`fenics.list_krylov_solver_preconditioners` to list 
    all the methods. This option applies only to iterative solvers. This item is optional. If not specified, a default 
    will be used where applicable.
    """

    def compile(self) -> typing.Dict[str, str]:
        """
        Compile the settings in a dictionary that can be passed to the ``solver_parameters`` argument of
        :func:`fenics.solve`.

        :return: Compiled arguments.
        :rtype: ``Dict[str, str]``
        """
        solver_parameters = {
            'linear_solver': self.linear_solver,
        }

        if self.preconditioner is not None:
            solver_parameters['preconditioner'] = self.preconditioner

        return solver_parameters


def solve(
        mesh: fenics.Mesh,
        simulation_settings: SimulationSettings,
        main_domain_settings: MainDomainSettings,
        solver_settings: SolverSettings,
        element_settings: ElementSettings = ElementSettings(),
        uniform_robin_bcs: UniformRobinBCS = UniformRobinBCS(),
        uniform_neumann_bcs: UniformNeumannBCS = UniformNeumannBCS(),
        uniform_dirichlet_bcs: UniformDirichletBCS = UniformDirichletBCS(),
) -> fenics.Function:
    """
    Solve the Helmholtz equation. For the equation definition see :ref:`theory`.

    :param mesh: The mesh of the domain.
    :type mesh: :class:`fenics.Mesh`
    :param simulation_settings: The settings of the simulation.
    :type simulation_settings: :class:`acoupy_helmholtz.solver.SimulationSettings`
    :param main_domain_settings: The settings of the main domain.
    :type main_domain_settings: :class:`acoupy_helmholtz.solver.MainDomainSettings`
    :param solver_settings: The solver settings.
    :type solver_settings: :class:`acoupy_helmholtz.solver.SolverSettings`
    :param element_settings: The settings of the element (optional). If not given defaults are used.
    :type element_settings: :class:`acoupy_helmholtz.solver.ElementSettings`
    :param uniform_robin_bcs: The uniform robin boundary conditions (optional). If not given none are applied.
    :type uniform_robin_bcs: :class:`acoupy_helmholtz.solver.UniformRobinBCS`
    :param uniform_neumann_bcs: The uniform Neumann boundary conditions (optional). If not given none are applied.
    :type uniform_neumann_bcs: :class:`acoupy_helmholtz.solver.UniformNeumannBCS`
    :param uniform_dirichlet_bcs: The uniform Dirichlet boundary conditions (optional). If not given none are applied.
    :type uniform_dirichlet_bcs: :class:`acoupy_helmholtz.solver.UniformDirichletBCS`
    :return: The solution.
    :rtype: :class:`fenics.Function`
    """

    base_element = fenics.FiniteElement(
        family=element_settings.family,
        cell=mesh.ufl_cell(),
        degree=element_settings.degree
    )
    element = fenics.MixedElement([base_element, base_element])
    f_space = fenics.FunctionSpace(mesh, element)

    trial_re, trial_im = fenics.TrialFunction(V=f_space)
    test_re, test_im = fenics.TestFunction(V=f_space)

    wave_number = 2.0 * np.pi * simulation_settings.frequency / simulation_settings.medium_settings.speed_of_sound
    gamma = (
            1.0 +
            1.0j * wave_number * simulation_settings.medium_settings.damping_eta /
            simulation_settings.medium_settings.speed_of_sound
    )
    beta = (
            wave_number ** 2 -
            1.0j * simulation_settings.medium_settings.damping_alpha /
            simulation_settings.medium_settings.speed_of_sound
    )
    flux_constant = (
            1.0j * 2.0 * np.pi * simulation_settings.frequency * simulation_settings.medium_settings.density * gamma
    )

    bilinear_form = (
            gamma.real * fenics.dot(fenics.grad(trial_re), fenics.grad(test_re)) * main_domain_settings.measure +
            gamma.real * fenics.dot(fenics.grad(trial_im), fenics.grad(test_im)) * main_domain_settings.measure +
            gamma.real * fenics.dot(fenics.grad(trial_im), fenics.grad(test_re)) * main_domain_settings.measure -
            gamma.real * fenics.dot(fenics.grad(trial_re), fenics.grad(test_im)) * main_domain_settings.measure +
            gamma.imag * fenics.dot(fenics.grad(trial_re), fenics.grad(test_re)) * main_domain_settings.measure +
            gamma.imag * fenics.dot(fenics.grad(trial_im), fenics.grad(test_im)) * main_domain_settings.measure -
            gamma.imag * fenics.dot(fenics.grad(trial_im), fenics.grad(test_re)) * main_domain_settings.measure +
            gamma.imag * fenics.dot(fenics.grad(trial_re), fenics.grad(test_im)) * main_domain_settings.measure +
            -beta.real * trial_re * test_re * main_domain_settings.measure +
            -beta.real * trial_im * test_im * main_domain_settings.measure +
            -beta.real * trial_im * test_re * main_domain_settings.measure -
            -beta.real * trial_re * test_im * main_domain_settings.measure +
            -beta.imag * trial_re * test_re * main_domain_settings.measure +
            -beta.imag * trial_im * test_im * main_domain_settings.measure -
            -beta.imag * trial_im * test_re * main_domain_settings.measure +
            -beta.imag * trial_re * test_im * main_domain_settings.measure
    )

    linear_form = 0.0

    for bc in uniform_robin_bcs.bcs:
        g_rb = flux_constant / bc.impedance

        bilinear_form += (
                g_rb.real * trial_re * test_re * bc.measure +
                g_rb.real * trial_im * test_im * bc.measure +
                g_rb.real * trial_im * test_re * bc.measure -
                g_rb.real * trial_re * test_im * bc.measure +
                g_rb.imag * trial_re * test_re * bc.measure +
                g_rb.imag * trial_im * test_im * bc.measure -
                g_rb.imag * trial_im * test_re * bc.measure +
                g_rb.imag * trial_re * test_im * bc.measure
        )

        if bc.velocity != (0.0 + 0.0j):
            uniform_neumann_bcs.bcs.append(
                UniformNeumannBC(
                    velocity=bc.velocity,
                    measure=bc.measure
                )
            )

    for bc in uniform_neumann_bcs.bcs:
        g_nm = flux_constant * bc.velocity

        linear_term = (
                g_nm.real * test_re * bc.measure +
                g_nm.imag * test_im * bc.measure +
                g_nm.imag * test_re * bc.measure -
                g_nm.real * test_im * bc.measure
        )

        if linear_form == 0.0:
            linear_form = linear_term
        else:
            linear_form += linear_term

    dirichlet_bcs = []
    for bc in uniform_dirichlet_bcs.bcs:
        dirichlet_bcs.append(
            fenics.DirichletBC(
                f_space.sub(0), bc.pressure.real, bc.subdomain
            )
        )
        dirichlet_bcs.append(
            fenics.DirichletBC(
                f_space.sub(1), bc.pressure.imag, bc.subdomain
            )
        )

    u = fenics.Function(f_space)

    fenics.solve(
        bilinear_form == linear_form,
        u,
        bcs=dirichlet_bcs,
        solver_parameters=solver_settings.compile(),
    )

    return u
